/*
** lem-in.h for lem-in in /home/camill_n/rendu/prog_elem/lem-in
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Apr  6 19:13:23 2014 camill_n
** Last update Thu Apr 24 18:01:08 2014 Maxime Boguta
*/

#ifndef LEM_IN_H_
# define LEM_IN_H_

# define START_CHAR "##start"
# define END_CHAR "##end"
# define COMMENT_CHAR "#"
# define ROOM data->tab_room

typedef struct		s_solve
{
  int			size;
  int			*path;
}			t_solve;

typedef struct		s_graph
{
  int			nb_fourmiz;
  char			*name;
  int			rec_check;
  int			type;
  int			coords[2];
  int			*join;
  int			tun;
}			t_graph;

typedef struct		s_data
{
  int			nb_solve;
  int			room;
  int			tot_fourmiz;
  int			cpt_line;
  int			size_max;
  int			wished_size;
  t_graph		**tab_room;
  t_graph		*enter;
  t_graph		*end;
  t_solve		**solve;
}			t_data;

void	get_nb_fourmiz(t_data *data, char *line, int *state);
void	show_room(t_data *data);
void	add_room(t_data *data, char **tab, int type);
void	add_tun(t_data *data, char **tab);
void	fatal_error(t_data *data, int errcode, char *line, int i);
void	solve(t_data *data);
void	check_room(t_graph *room, t_data *data, int i);
void	get_graph(t_data *data);
void	get_errors(t_data *data);
void	check_errors(t_data *data, int);
void	check_room(t_graph *room, t_data *data, int);
void	check_tunnel(t_data *data, int);
void	get_errors(t_data *data);
void	get_path(t_data *data);
void	disp_solve_id(t_data *data);
int	get_id_start_room(t_data *data);
int	get_size(int *buff, int size, int needle);
int	get_smaller_solve(t_data *data, int size_needle, int c_id, int *ptr_size);

#endif
