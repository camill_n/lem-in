/*
** alloc.h for my_select in /home/camill_n/rendu/PSU_2013_my_select
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Wed Jan  8 18:29:24 2014 Nicolas Camilli
** Last update Wed Jan  8 18:54:25 2014 Nicolas Camilli
*/

#ifndef ALLOC_H_
# define ALLOC_H_

void	*x_malloc(int size, char *name_var);

#endif
