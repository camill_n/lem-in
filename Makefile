##
## Makefile for test in /home/camill_n/rendu/Piscine-C-lib/my
##
## Made by Nicolas Camilli
## Login   <camill_n@epitech.net>
##
## Started on  Mon Oct 21 07:17:19 2013 Nicolas Camilli
## Last update Thu Apr 24 17:46:29 2014 Maxime Boguta
##

SRCS	= 	src/main.c \
		src/init.c \
		src/alloc.c \
		src/wordtab.c \
		src/wordtab2.c \
		src/utils.c \
		src/room.c \
		src/errors.c \
		src/solve.c \
		src/solve_utils2.c \
		src/solve_utils.c \

RM	= rm -f

NAME	= lem-in

AR	= ar rc

CC	= cc -Wall -g3

OBJS	= $(SRCS:.c=.o)

CFLAGS	= -I./include/

all:	$(NAME)

.c.o:
	$(CC) $(CFLAGS) -c $< -o $(<:.c=.o)

$(NAME): $(OBJS)
	 $(CC) $(OBJS) -o $(NAME)

clean:
	$(RM) $(OBJS)

fclean:	clean
	$(RM) $(NAME)

re: fclean all
