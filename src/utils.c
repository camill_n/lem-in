/*
** utils.c for lem-in in /home/camill_n/rendu/prog_elem/lem-in
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon Apr  7 18:30:28 2014 camill_n
** Last update Tue Apr 15 15:46:44 2014 camill_n
*/

#include <stdio.h>
#include "lem-in.h"

void	show_join(int n, int *join)
{
  int	i;

  i = 0;
  while (i < n)
    {
      printf("Liaison avec la salle: %d\n", join[i]);
      ++i;
    }
  printf("\n");
}

void	show_room(t_data *data)
{
  int	i;

  i = 0;
  while (data->tab_room[i] != NULL)
    {
      printf("ID: %d\nnb_fourmiz: %d\nname: %s\ntype: %d\nX: %d\nY: %d\n\n\n",
  	     i, data->tab_room[i]->nb_fourmiz,
	     ROOM[i]->name, ROOM[i]->type,
	     ROOM[i]->coords[0], ROOM[i]->coords[1]);
      show_join(ROOM[i]->tun, ROOM[i]->join);
      ++i;
    }
}
