/*
** solve.c for lem-in in /home/camill_n/rendu/prog_elem/lem-in
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Apr 15 23:45:09 2014 camill_n
** Last update Thu Apr 24 18:09:49 2014 Maxime Boguta
*/

#include <stdio.h>
#include <stdlib.h>
#include "lem-in.h"
#include "alloc.h"

int	get_next_id(t_data *data, int solve_id, int c_id)
{
  int	i;

  i = data->solve[solve_id]->size - 1;
  while (i >= 0)
    {
      if (c_id == data->solve[solve_id]->path[i]
	  && data->solve[solve_id]->path[i + 1] != -1)
	return (data->solve[solve_id]->path[i + 1]);
      --i;
    }
  return (-1);
}

void	decide_for(t_data *data, int *fourmiz, int current_ant)
{
  int	best_id;
  int	size;
  int	next_id;

  best_id = get_smaller_solve(data, data->wished_size,
			      fourmiz[current_ant], &size);
  next_id = get_next_id(data, best_id, fourmiz[current_ant]);
  if (next_id != -1 &&
      (ROOM[next_id]->type == 2 ||
       (ROOM[fourmiz[current_ant]]->type != 2
	&& ROOM[next_id]->nb_fourmiz == 0)))
    {
      ++ROOM[next_id]->nb_fourmiz;
      --ROOM[fourmiz[current_ant]]->nb_fourmiz;
      fourmiz[current_ant] = next_id;
      printf("Je suis la fourmi numero %d ", current_ant);
      printf("et je me dirige actuellement vers la salle %d!\n", next_id);
      if (ROOM[next_id]->type == 2)
	printf("Ouf je suis enfin parvenue a la sortie !! =D\n");
    }
  else
    printf("Je suis la fourmi numero %d et je reste sur %d\n",
	   current_ant, fourmiz[current_ant]);
  data->wished_size = size;
  data->size_max = -1;
}

void	resolve_path(t_data *data, int *fourmiz)
{
  int	cycle;
  int	i;

  cycle = 0;
  while (data->end->nb_fourmiz != data->tot_fourmiz)
    {
      i = 0;
      while (i < data->tot_fourmiz)
	{
	  data->wished_size = -1;
	  decide_for(data, fourmiz, i);
	  ++i;
	}
      ++cycle;
    }
}

void	solve(t_data *data)
{
  int	*fourmiz;
  int	i;
  int	start;

  i = 0;
  get_path(data);
  disp_solve_id(data);
  if (data->nb_solve == 0)
    {
      printf("No path to the end for the poors ants...\n");
      exit(0);
    }
  fourmiz = x_malloc(data->tot_fourmiz * sizeof(int), "fourmiz");
  start = get_id_start_room(data);
  while (i < data->tot_fourmiz)
    {
      fourmiz[i] = start;
      ++i;
    }
  resolve_path(data, fourmiz);
}
