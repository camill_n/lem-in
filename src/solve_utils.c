/*
** solve_utils.c for lem-in in /home/camill_n/rendu/prog_elem/lem-in
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu Apr 17 16:39:36 2014 camll_n
** Last update Thu Apr 24 18:06:25 2014 Maxime Boguta
*/

#include <stdlib.h>
#include <stdio.h>
#include "lem-in.h"
#include "alloc.h"

void	disp_solve_id(t_data *data)
{
  int		i;
  int		j;

  j = 0;
  printf("Il y a %d chemins possibles\n", data->nb_solve);
  while (j < data->nb_solve)
    {
      printf("-> (size: %d) ", data->solve[j]->size);
      i = 0;
      while (i < data->solve[j]->size)
	{
	  printf("%d ", data->solve[j]->path[i]);
	  if (i != data->solve[j]->size - 1)
	    printf("- ");
	  ++i;
	}
      printf("\n");
      ++j;
    }
}

void	add_solve(t_data *data, int *solve_id, int size)
{
  int	i;

  i = 0;
  data->solve = realloc(data->solve,
			(data->nb_solve + 1) * sizeof(t_solve *));
  data->solve == NULL ? exit(0) : 0;
  data->solve[data->nb_solve] = x_malloc(sizeof(t_solve), "solve");
  data->solve[data->nb_solve]->size = size;
  data->solve[data->nb_solve]->path
    = x_malloc((size + 1) * sizeof(int), "solve");
  while (i < size)
    {
      data->solve[data->nb_solve]->path[i] = solve_id[i];
      ++i;
    }
  data->solve[data->nb_solve]->path[i] = -1;
  ++data->nb_solve;
}

char	recursive_solve(t_data *data, int current_room, int *solve_id, int pos)
{
  int	i;
  char	tun;

  solve_id[pos] = current_room;
  if (ROOM[current_room]->type == 2)
    {
      add_solve(data, solve_id, pos + 1);
      return (1);
    }
  tun = ROOM[current_room]->tun;
  ROOM[current_room]->rec_check = 1;
  i = 0;
  while (i < tun)
    {
      if (ROOM[ROOM[current_room]->join[i]]->rec_check == 0)
	recursive_solve(data, ROOM[current_room]->join[i], solve_id, pos + 1);
      ++i;
    }
  ROOM[current_room]->rec_check = 0;
  return (0);
}

int	get_id_start_room(t_data *data)
{
  int	id;

  id = 0;
  while (ROOM[id] != NULL)
    {
      if (ROOM[id]->type == 1)
	return (id);
      ++id;
    }
  return (-1);
}

void	get_path(t_data *data)
{
  int	id_start_room;
  int	*solve_id;

  data->nb_solve = 0;
  data->solve = NULL;
  solve_id = x_malloc((data->room + 1) * sizeof(int), "solve_id");
  id_start_room = get_id_start_room(data);
  recursive_solve(data, id_start_room, solve_id, 0);
  free(solve_id);
}
