/*
** room.c for lem-in in /home/camill_n/rendu/prog_elem/lem-in
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon Apr  7 18:36:48 2014 camill_n
** Last update Wed Apr 23 18:26:58 2014 camill_n
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lem-in.h"
#include "wordtab.h"
#include "alloc.h"

t_graph		*set_room(t_data *data, char **tab, int type)
{
  t_graph	*graph;

  graph = x_malloc(sizeof(t_graph), "graph");
  graph->rec_check = 0;
  graph->nb_fourmiz = 0;
  graph->name = strdup(tab[0]);
  graph->type = type;
  graph->coords[0] = atoi(tab[1]);
  graph->coords[1] = atoi(tab[2]);
  graph->join = NULL;
  graph->tun = 0;
  if (type == 1)
    {
      data->enter = graph;
      graph->nb_fourmiz = data->tot_fourmiz;
    }
  if (type == 2)
    data->end = graph;
  return (graph);
}

void		add_room(t_data *data, char **tab, int type)
{
  ROOM = realloc(data->tab_room, (data->room + 1) * sizeof(t_graph *));
  if (data->tab_room == NULL)
    {
      printf("Memory access denied\n");
      exit(EXIT_FAILURE);
    }
  ROOM[data->room] = set_room(data, tab, type);
  check_room(ROOM[data->room], data, data->room);
  ++(data->room);
}

void		set_tun(t_data *data, int i, char **tab)
{
  int		j;
  int		save;
  int		size_to_realloc;

  j = 0;
  save = -1;
  while (j < data->room && save == -1)
    {
      if (strcmp(ROOM[j]->name, tab[1]) == 0)
	save = j;
      ++j;
    }
  if (save == -1)
    fatal_error(data, 4, tab[1], 0);
  if (save != i)
    {
      size_to_realloc = (ROOM[i]->tun + 1) * sizeof(int *);
      ROOM[i]->join = realloc(ROOM[i]->join, size_to_realloc);
      size_to_realloc = (ROOM[save]->tun + 1) * sizeof(int *);
      ROOM[save]->join = realloc(ROOM[save]->join, size_to_realloc);
      if (ROOM[i]->join == NULL || ROOM[save]->join == NULL)
	{
	  printf("Memory access denied\n");
	  exit(0);
	}
      ROOM[i]->join[ROOM[i]->tun] = save;
      ROOM[save]->join[ROOM[save]->tun] = i;
      check_tunnel(data, i);
      ++ROOM[i]->tun;
      ++ROOM[save]->tun;
    }
}

void		add_tun(t_data *data, char **tab)
{
  int		i;
  int		check;

  check = 0;
  i = 0;
  while (i < data->room && !check)
    {
      if (strcmp(ROOM[i]->name, tab[0]) == 0)
	{
	  set_tun(data, i, tab);
	  ++check;
	}
      ++i;
    }
  (!check) ? fatal_error(data, 4, tab[0], 0) : 0;
}
