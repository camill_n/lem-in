/*
** solve_utils2.c for lem-in in /home/camill_n/rendu/prog_elem/lem-in/lem-in
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu Apr 24 12:18:04 2014 camill_n
** Last update Thu Apr 24 17:23:18 2014 camill_n
*/

#include <stdio.h>
#include "lem-in.h"

int	get_size(int *buff, int size, int needle)
{
  int	cpt;

  cpt = 0;
  while (size >= 0)
    {
      if (buff[size] == needle)
	return (cpt);
      --size;
      ++cpt;
    }
  return (-1);
}

void	init_varia(int *i, int *size, int *ret)
{
  *i = -1;
  *size = -1;
  *ret = -1;
}

int	get_smaller_solve(t_data *data, int size_needle, int c_id, int *ptr_size)
{
  int	size;
  int	i;
  int	ret;
  int	tmp;

  init_varia(&i, &size, &ret);
  while (++i < data->nb_solve)
    {
      tmp = get_size(data->solve[i]->path, data->solve[i]->size - 1, c_id);
      tmp > data->size_max ? data->size_max = tmp : 0;
      if (size_needle != -1 && size_needle == tmp)
	{
	  *ptr_size = tmp;
	  ret = i;
	  return (ret);
	}
      if ((size > tmp || size == -1) && tmp != -1)
	{
	  size = tmp;
	  *ptr_size = tmp;
	  ret = i;
	}
    }
  return (ret);
}
