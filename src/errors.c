/*
** errors.c for errors in /home/camill_n/rendu/prog_elem/lem-in
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Apr 15 13:51:21 2014 camill_n
** Last update Wed Apr 23 18:39:23 2014 camill_n
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "lem-in.h"

void	check_errors(t_data *data, int errcode)
{
  printf("Error : ");
  if (errcode == 102)
    printf("defining start room entry twice (or more)\n");
  if (errcode == 103)
    printf("defining end room entry twice (or more)\n");
  if (errcode == 1)
    printf("missing start room definition\n");
  if (errcode == 2)
    printf("missing end room definition\n");
  if (errcode == 3)
    printf("nothing to read\n");
  exit(EXIT_FAILURE);
}

void	fatal_error(t_data *data, int errcode, char *line, int i)
{
  if (errcode < 100)
    printf("Fatal error on line %d\n> %s\n", data->cpt_line, line);
  if (errcode > 100)
    printf("Fatal error on room %d\n> name : %s\n", i
	   , data->tab_room[i]->name);
  if (errcode == 0)
    printf("Expecting ant number given as following : 'nb_ant\\n'\n");
  if (errcode == 1)
    printf("Expecting room declaration as following : 'name x y\\n'\n");
  if (errcode == 2)
    printf("Expecting tunnel declaration as following : 'name1-name2\n'\\n");
  if (errcode == 3)
    printf("Uknown line type\n");
  if (errcode == 4)
    printf("Incorrect room declaration/call\n");
  if (errcode == 101)
    printf("Error : a room name is declared twice (or more)\n");
  if (errcode == 104)
    printf("Error : two rooms (or more) are set at the same position\n");
  if (errcode == 105)
    printf("Error : same tunnel is declared twice (or more)\n");
  exit(EXIT_FAILURE);
}

void	get_errors(t_data *data)
{
  if (data->enter == NULL)
    check_errors(data, 1);
  if (data->end == NULL)
    check_errors(data, 2);
}

void	check_room(t_graph *room, t_data *data, int nb)
{
  int	i;

  i = -1;
  while (++i < data->room)
    {
      if (strcmp(room->name, data->tab_room[i]->name) == 0)
	fatal_error(data, 101, NULL, nb);
      if (room->type == 1 && data->tab_room[i]->type == 1)
	fatal_error(data, 102, NULL, nb);
      if (room->type == 2 && data->tab_room[i]->type == 2)
	fatal_error(data, 103, NULL, nb);
      if (room->coords[0] == data->tab_room[i]->coords[0]
	  && room->coords[1] == data->tab_room[i]->coords[1])
	fatal_error(data, 104, NULL, nb);
    }
}

void	check_tunnel(t_data *data, int i)
{
  /* int	j; */
  /* int	ref; */

  /* j = -1; */
  /* ref = ROOM[i]->join[ROOM[i]->tun]; */
  /* while (++j < ROOM[i]->tun) */
  /*   { */
  /*     if (ROOM[i]->join[j] == ref) */
  /* 	fatal_error(data, 105, NULL, i); */
  /*   } */
}
