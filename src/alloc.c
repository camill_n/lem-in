/*
** alloc.c for lib in /home/camill_n/rendu/PSU_2013_my_select/lib/src
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Wed Jan  8 18:13:56 2014 Nicolas Camilli
** Last update Mon Apr  7 15:16:42 2014 camill_n
*/

#include <stdlib.h>
#include <stdio.h>

void	*x_malloc(int size, char *name_var)
{
  void	*var;

  var = malloc(size);
  if (var == NULL)
    {
      printf("Memory access denied for var %s\n", name_var);
      exit(0);
    }
  return (var);
}
