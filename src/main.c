/*
** main.c for lem-in in /home/camill_n/rendu/prog_elem/lem-in
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Apr  6 19:09:40 2014 camill_n
** Last update Wed Apr 23 18:17:46 2014 camill_n
*/

#include <stdio.h>
#include <stdlib.h>
#include "lem-in.h"

void		my_free(t_data *data)
{
  int		i;

  i = 0;
  while (data->tab_room[i] != NULL)
    {
      free(ROOM[i]->join);
      free(data->tab_room[i]->name);
      free(data->tab_room[i++]);
    }
  free(data->tab_room[i]);
  free(data->tab_room);
}

int		main(int ac, char **av)
{
  t_data	data;

  get_graph(&data);
  show_room(&data);
  get_errors(&data);
  solve(&data);
  my_free(&data);
  return (0);
}
