/*
** wordtab.c for mysh in /home/camill_n/rendu/PSU_2013_minishell1
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Mon Dec  9 00:24:36 2013 Nicolas Camilli
** Last update Tue Apr  8 14:42:43 2014 camill_n
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lem-in.h"

int	get_nb_word(char *str, char c)
{
  int	i;
  int	cpt;

  cpt = 1;
  i = 0;
  while (str[i] != '\0')
    {
      (str[i] == ' ' && str[i + 1] != ' ') ? ++cpt : 0;
      (str[i] == c && str[i + 1] != c) ? ++cpt : 0;
      ++i;
    }
  return (cpt);
}

void	my_free_wordtab(char **tab)
{
  int	i;

  i = 0;
  if (tab != NULL)
    {
      while (tab[i] != NULL)
	{
	  free(tab[i]);
	  ++i;
	}
      free(tab[i]);
      free(tab);
    }
}

void	my_show_wordtab(char **tab)
{
  int	i;

  i = 0;
  while (tab[i] != NULL)
    {
      printf("%s\n", tab[i]);
      ++i;
    }
}

int	get_sizetab(char **tab)
{
  int	i;

  i = 0;
  while (tab[i] != NULL)
    ++i;
  return (i);
}

char	**duptab(char **tab)
{
  int	i;
  char	**new_tab;
  int	size;

  size = get_sizetab(tab) + 1;
  new_tab = malloc(size * sizeof(char *));
  if (new_tab == NULL)
    {
      printf("Memory access denied\n");
      exit(0);
    }
  i = 0;
  while (tab[i] != NULL)
    {
      new_tab[i] = strdup(tab[i]);
      ++i;
    }
  new_tab[i] = NULL;
  return (new_tab);
}
