/*
** init.c for lem-in in /home/camill_n/rendu/prog_elem/lem-in
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon Apr  7 14:55:56 2014 camill_n
** Last update Thu Apr 24 12:54:24 2014 camill_n
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lem-in.h"
#include "wordtab.h"
#include "alloc.h"

void	get_nb_fourmiz(t_data *data, char *line, int *state)
{
  data->tot_fourmiz = atoi(line);
  if (data->tot_fourmiz <= 0)
    {
      printf("Error : ant amount must be a positive number\n");
      exit(EXIT_FAILURE);
    }
  *state += 1;
}

int		verif_type(char *line, t_data *data)
{
  int		ret;

  ret = 0;
  if (strcmp(line, START_CHAR) == 0)
    ret = 1;
  else if (strcmp(line, END_CHAR) == 0)
    ret = 2;
  else if (line[0] == '#')
    ret = 3;
  if (ret == 1)
    if (data->enter)
      check_errors(data, 102);
  if (ret == 2)
    if (data->end)
      check_errors(data, 103);
  return (ret);
}

void	init_var(char **buff, int *state, size_t *read, t_data *data)
{
  buff[0] = NULL;
  *state = 0;
  *read = 512;
  data->cpt_line = 0;
  data->room = 0;
  data->enter = NULL;
  data->end = NULL;
  data->tab_room = NULL;
  data->size_max = -1;
}

void		check(t_data *data, char *buff, int *state, int type)
{
  char		**tab;
  int		size;

  ((type == 1 || type == 2) && *state != 1) ?
    fatal_error(data, 3, buff, 0) : 0;
  if (verif_type(buff, data) == 0)
    {
      tab = my_wordtab(buff, '-');
      size = get_sizetab(tab);
      if (size != 0)
	{
	  (*state == 0 && size != 1) ? fatal_error(data, 0, buff, 0) : 0;
	  (*state == 1 && size == 2) ? ++(*state) : 0;
	  (*state == 1 && size != 3) ? fatal_error(data, 1, buff, 0) : 0;
	  (*state == 2 && size != 2) ? fatal_error(data, 2, buff, 0) : 0;
	  (*state == 1) ? add_room(data, tab, type) : 0;
	  (*state == 2) ? add_tun(data, tab) : 0;
	  (*state == 0) ? get_nb_fourmiz(data, buff, state) : 0;
	}
      my_free_wordtab(tab);
    }
}

void		get_graph(t_data *data)
{
  char		*buff;
  size_t	read;
  int		state;
  int		ret;
  int		tot;
  int		type;

  init_var(&buff, &state, &read, data);
  type = 0;
  tot = 0;
  while ((ret = getline(&buff, &read, stdin)) > 0)
    {
      tot += ret;
      ++data->cpt_line;
      buff[ret - 1] == '\n' ? buff[ret - 1] = 0 : 0;
      check(data, buff, &state, type);
      type = verif_type(buff, data);
      free(buff);
      buff = NULL;
    }
  tot == 0 ? check_errors(data, 3) : 0;
  ROOM = realloc(data->tab_room, (data->room + 1) * sizeof(t_graph *));
  ROOM[data->room] = NULL;
  (buff != NULL) ? free(buff) : 0;
}
